import React, { useContext } from 'react';
import styled from 'styled-components';
import { Context } from '../context/context';

const Wrapper = styled.div`
    width: 10rem;
    height: 2rem;
    border-radius: 10px;
    background: #00ffff;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    border:1px solid #00aaaa;
    -webkit-user-select: none;  
    margin: 20px;
    padding: 10px;
    &:hover{
        background: #00eeee;  
    }
`;
interface Props {
    rate: number;
    count: number;
}
const Button = (props: Props) => {
    const {increaseCount, setCount} = useContext(Context);
    return (
        <Wrapper onClick={() => {setCount(props.count + props.rate)}}>Нажми на меня +{props.rate}</Wrapper>
    );
}

export default Button;