import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    width: 10rem;
    height: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    border:1px solid #00aaaa;
    -webkit-user-select: none;  
`;
interface Props {
    count: number;
}

const Display = (props: Props) => {
    return (
        <Wrapper>{props.count}</Wrapper>
    );
}

export default Display;