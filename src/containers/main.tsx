import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Button from '../components/button';
import Display from '../components/display';
import { Context } from '../context/context';
import { FirebaseState } from '../context/firebase/firebaseState';
import { useDebounce } from 'react-use';
import { increaseCount, fetchCount } from '../api/count';

const Root = styled.div`
    height: 100vh;
    width: 100vw;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
`;

const Main = () => {
    const [count, setCount] = useState(0);

    useDebounce(()=> {increaseCount(count)},400,[count]);

    useEffect(() => {
        fetchCount().then(count => setCount(count.data));
      },
      []
      );
    return (
        <FirebaseState>
            <Context.Provider value={{
                setCount
            }}>
                <Root>
                    <h1>Click Me!</h1>
                    <Display count={count} />
                    <div>
                        <Button count={count} rate={1}/>
                        <Button count={count} rate={10}/>
                        <Button count={count} rate={100}/>
                    </div>
                </Root>
            </Context.Provider>
        </FirebaseState>
    );
}

export default Main;