import React from 'react';
import Main from './containers/main';


const App: React.FC = () => {
  return (
      <Main />
  );
}

export default App;
