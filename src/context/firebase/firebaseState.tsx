import React from 'react';
import {FirebaseContext} from './firebaseContext';

export const FirebaseState = ({children} : any)=> {
    return (
        <FirebaseContext.Provider>
            {children}
        </FirebaseContext.Provider>
    )
}