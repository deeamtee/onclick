import axios from 'axios';

const url = process.env.REACT_APP_DB_URL

export const fetchCount = async () => {
    return await axios.get(`${url}/count.json`);
};

export const increaseCount = async (count:number) => {
    await axios.put(`${url}/count.json`, count);
}